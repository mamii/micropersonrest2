package mami.micropersonrest.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import mami.micropersonrest.domain.Person;

/**
 *
 * @author maierm
 */
public class PersonRepository {

    public static final PersonRepository INSTANCE = new PersonRepository();

    private final Map<Integer, Person> id2Person = new ConcurrentHashMap<>();
    private final int INITIAL_COUNT = 1000;

    {
        for (int i = 0; i < INITIAL_COUNT; i++) {
            id2Person.put(i, new Person(i, "fn" + i, "ln" + i));
        }
    }

    private AtomicInteger sequenceNum = new AtomicInteger(INITIAL_COUNT);

    public List<Person> findAll() {
        return new ArrayList<>(id2Person.values());
    }

    public Person findById(Integer id) {
        return id2Person.get(id);
    }

    /**
     *
     * @param p
     * @return null, if already exists
     */
    public Person addPerson(Person p) {
        if (p.getId() != null && id2Person.get(p.getId()) != null) {
            return null;
        }
        Integer newId = sequenceNum.getAndIncrement();
        p.setId(newId);
        id2Person.put(newId, p);
        return p;
    }
}
