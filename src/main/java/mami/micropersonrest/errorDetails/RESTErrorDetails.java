package mami.micropersonrest.errorDetails;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author maierm
 */
public class RESTErrorDetails {

    public static final int ERR_ENTITY_ALREADY_EXISTS = -400;
    public static final int ERR_VALIDATION = -401;

    public static final RESTErrorDetails ALREADY_EXISTS = new RESTErrorDetails(-400, "entity already exists");

    private Integer errorCode;
    private String desc;
    //TODO: make Map<String, List<String>> as type ????
    private Map<String, List<String>> validationErrors = new TreeMap<>();

    private RESTErrorDetails(Integer errorCode, String desc) {
        this.errorCode = errorCode;
        this.desc = desc;
    }

    public static RESTErrorDetails fromValidationErrors(Map<String, List<String>> errors) {
        RESTErrorDetails res = new RESTErrorDetails(ERR_VALIDATION, "there were validation errors");
        res.validationErrors.putAll(errors);
        return res;
    }

    public RESTErrorDetails() {
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Map<String, List<String>> getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(Map<String, List<String>> validationErrors) {
        this.validationErrors = validationErrors;
    }

    @Override
    public String toString() {
        return "RESTErrorDetails{" + "errorCode=" + errorCode + ", desc=" + desc + ", validationErrors=" + validationErrors + '}';
    }

}
