package mami.micropersonrest;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import mami.micropersonrest.domain.Person;
import mami.micropersonrest.errorDetails.RESTErrorDetails;
import mami.micropersonrest.repository.PersonRepository;

@Path(PersonREST.PATH)
public class PersonREST {

    public static final String PATH = "persons";

    private PersonRepository getRepo() {
        return PersonRepository.INSTANCE;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Person> getAll() {
        return getRepo().findAll();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getById(@PathParam("id") Integer id) {
        Person result = getRepo().findById(id);
        if (result == null) {
            throw new NotFoundException();
        }
        return result;
    }

//    private String toJSON(Set constraintViolations) {
//        StringBuilder builder = new StringBuilder();
//        builder.append("[");
//        for (Object cv_ : constraintViolations) {
//            ConstraintViolation<?> cv = (ConstraintViolation<?>) cv_;
//            builder.append(String.format("\"%s\"", cv.getPropertyPath().toString()));
//            builder.append(":");
//            builder.append(String.format("\"%s\"", cv.getMessage())); // TODO: localize me?
//            builder.append(",");
//        }
//        if (builder.length() > 1) {
//            builder.deleteCharAt(builder.length() - 1);
//        }
//        builder.append("]");
//        return builder.toString();
//    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPerson(Person person) {
        Response res = validationErrorsResponse(person);
        if (res != null) {
            return res;
        }

        Person newPerson = getRepo().addPerson(person);
        if (newPerson == null) {
            return Response.status(409).entity(RESTErrorDetails.ALREADY_EXISTS).build();
        }
        return Response.created(URI.create(PersonREST.PATH + "/" + newPerson.getId())).entity(newPerson).build();
    }

    private Response validationErrorsResponse(Object o) {
        Map<String, List<String>> errors = validationErrors(o);
        if (errors.isEmpty()) {
            return null;
        }
        return Response.status(409).entity(RESTErrorDetails.fromValidationErrors(errors)).build();
    }

    private Map<String, List<String>> validationErrors(Object o) {
        Map<String, List<String>> errors = new HashMap<>();
        ValidatorFactory validatorFac = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFac.getValidator();
        Set vr = validator.validate(o);
        if (vr.size() > 0) {
            for (Object cv_ : vr) {
                ConstraintViolation cv = (ConstraintViolation) cv_;
                String key = cv.getPropertyPath().toString();
                List<String> values = errors.getOrDefault(key, new ArrayList<>());
                values.add(cv.getMessage());
                errors.put(key, values);
            }
        }
        return errors;
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatePerson(@PathParam("id") int id, Person update) {
        Response res = validationErrorsResponse(update);
        if (res != null) {
            return res;
        }

        Person current = getRepo().findById(id);
        if (current == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        current.update(update);
        return Response.noContent().build();
    }
}
