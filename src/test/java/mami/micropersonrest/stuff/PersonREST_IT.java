package mami.micropersonrest.stuff;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import mami.micropersonrest.domain.Person;
import mami.micropersonrest.errorDetails.RESTErrorDetails;
import org.glassfish.jersey.logging.LoggingFeature;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 * Simple REST-integration test. Perhaps we should try
 * https://www.logicbig.com/tutorials/java-ee-tutorial/jax-rs/jax-rs-unit-testing.html
 * instead?
 *
 * @author maierm
 */
public class PersonREST_IT {

    private static final Logger LOG = Logger.getLogger(PersonREST_IT.class.getName());
    private static final int port = 8888;
    static final String URL_PREFIX = String.format("http://localhost:%s/microPersonREST_v1", port);

    Client client;
    static Process payara;

    public PersonREST_IT() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        final String cmd = String.format("java -jar ./target/payara-micro.jar --deploy "
                + "./target/microPersonREST_v1.war --port %s", port);
        String[] splitted = cmd.split("\\s+");

        ProcessBuilder pb = new ProcessBuilder(splitted);
        payara = pb.start();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(payara.getErrorStream()))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                System.err.println(line);
                if (line.matches("(.*)#badassmicrofish (.*) ready in(.*)")) {
                    break;
                }
            }
        }
    }

    @AfterClass
    public static void tearDownClass() {
        payara.destroy();
        while (payara.isAlive());
        LOG.info("payara exit value: " + payara.exitValue());
    }

    @Before
    public void setUp() {
        client = ClientBuilder.newClient();
        Feature feature = new LoggingFeature(LOG, Level.INFO, LoggingFeature.Verbosity.PAYLOAD_ANY, null);
        client.register(feature);
    }

    @After
    public void tearDown() {
    }

    private Person get(int id) {
        Person p = client.target(URL_PREFIX + "/api/persons/" + id).request()
                .accept(MediaType.APPLICATION_JSON)
                .get(Person.class);
        return p;
    }

    private Response put(Person p) {
        Response response = client.target(URL_PREFIX + "/api/persons/" + p.getId())
                .request().put(Entity.json(p));
        return response;
    }

    private Response post(Person p) {
        Response response = client.target(URL_PREFIX + "/api/persons")
                .request().post(Entity.json(p));
        return response;
    }

    @Test
    public void testPUT() {
        Person p = get(1);
        p.setFirstName("newfirstname");
        assertEquals(204, put(p).getStatus());

        p = get(1);
        assertEquals("newfirstname", p.getFirstName());
        p.setFirstName("fn1");
        assertEquals(204, put(p).getStatus());
    }

    @Test
    public void testPOST() {
        String fn = "testfn" + new Date().getTime();
        Person p = new Person(fn, "testln");
        Response response = post(p);

        assertEquals(201, response.getStatus());
        Person created = response.readEntity(Person.class);
        assertNotNull(created.getId());
        created.setId(null);
        assertEquals(p, created);
        String location = response.getLocation().toString();
        assertTrue(location.startsWith(URL_PREFIX));
        response.close();
    }

    @Test
    public void testPOST_validationErrors() {
        Person p = new Person("A", "b");
        Response response = post(p);
        assertEquals(409, response.getStatus());
        
        RESTErrorDetails details = response.readEntity(RESTErrorDetails.class);
        assertEquals(RESTErrorDetails.ERR_VALIDATION, details.getErrorCode().intValue());
        Map<String, List<String>> errors = details.getValidationErrors();
        assertTrue(errors.containsKey("firstName"));
        assertTrue(errors.containsKey("lastName"));
        assertEquals(2, errors.get("firstName").size());        
        response.close();
    }

    @Test
    public void testPOST_alreadyExists() {
        String fn = "testfn" + new Date().getTime();
        Person p = new Person(1, fn, "testln");
        Response response = post(p);
        RESTErrorDetails error = response.readEntity(RESTErrorDetails.class);
        assertEquals(409, response.getStatus());
        assertEquals(-400, error.getErrorCode().longValue());
        LOG.info(error.toString());
    }

}
